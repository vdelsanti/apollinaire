apollinaire
=======================================

**apollinaire** is a Python implementation of helio- and asteroseismic MCMC
peakbagging methods. You will find here all the necessary tools in order to
analyse the acoustic oscillations of your favourite solar-like star. 

The source code is hosted on a `GitLab repository
<https://gitlab.com/sybreton/apollinaire>`_ and is still in development. A
paper detailing the complete modelling framework and benchmarking the code
performances is also in the process of writing !  

.. image:: https://anaconda.org/conda-forge/apollinaire/badges/version.svg/?style=plastic
.. image:: https://anaconda.org/conda-forge/apollinaire/badges/license.svg/?style=plastic  
.. image:: https://anaconda.org/conda-forge/apollinaire/badges/installer/conda.svg

|

.. toctree::
   :maxdepth: 1
   :caption: User guide

   usage/installation
   usage/quickstart/first_steps
   usage/advanced/advanced_peakbagging
   usage/format
   usage/synthetic_spectrum/synthetic_spectrum
   usage/quality_assurance/quality_assurance

.. toctree::
   :maxdepth: 2
   :caption: Detailed API

   usage/psd_module
   usage/peakbagging_module
   usage/sim_module
   usage/songlib

