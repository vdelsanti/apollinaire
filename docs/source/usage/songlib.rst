The songlib module
******************

Additionnally to its power spectrum managing and peakbagging tools,
**apollinaire** provides a reduction pipeline for data obtained with
the Solar-SONG instrument (Grundahl et al. 2007, Pallé et al. 2016,
Fredslund Andersen et al. 2019, Breton et al. in prep).

.. autofunction:: apollinaire.songlib.standard_correction

